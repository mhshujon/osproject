<?php include_once ('header.php');
session_start();
?>

    <div class="container">
        <?php if (!empty($_POST['totalProcess']) || !empty($_SESSION['totalProcess'])):?>
            <?php
            if (empty($_POST['totalProcess']))
                $_POST['totalProcess'] = $_SESSION['totalProcess'];
            ?>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation container" action="scheduling.php" method="POST">
                        <table class="table">
                            <thead class="black white-text">
                            <tr>
                                <th scope="col">Process</th>
                                <th scope="col">Arrival Time</th>
                                <th scope="col">Burst Time</th>
                                <th scope="col">Priority</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php for ($i=1; $i<=$_POST['totalProcess']; $i++):?>
                                <tr>
                                    <th scope="row">P<?php echo $i?></th>
                                    <td>
                                        <select class="mdb-select" required name="arrivalTimeP<?php echo $i?>">
                                            <option value="0" selected>0</option>
                                            <?php for ($j=1; $j<=30; $j++):?>
                                                <option value="<?php echo $j?>"><?php echo $j?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="mdb-select" required name="burstTimeP<?php echo $i?>">
                                            <option value="" disabled selected>Burst Time</option>
                                            <?php for ($j=0; $j<=30; $j++):?>
                                                <option value="<?php echo $j?>"><?php echo $j?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                    <td>
                                        <select class="mdb-select" required name="priority<?php echo $i?>">
                                            <option value="0" selected>0</option>
                                            <?php for ($j=1; $j<=$_POST['totalProcess']; $j++):?>
                                                <option value="<?php echo $j?>"><?php echo $j?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                            <?php endfor;?>
                            </tbody>
                        </table>
                        <div>
                            <select class="mdb-select" required name="schedulingProcess">
                                <option value="" disabled selected>Scheduling Process</option>
                                <option value="rr">Round Robin (RR)</option>
                                <option value="sjf">Shortest Job First (SJF)</option>
                                <option value="ps">Priority Scheduling</option>
                            </select>
                        </div>
                        <?php
                        if (empty($_SESSION['totalProcess']))
                            $_SESSION['totalProcess'] = $_POST['totalProcess'];
                        ?>
                        <button class="btn btn-primary btn-sm btn-rounded" type="submit">Submit</button>
                        <a href="restart.php">
                            <button class="btn btn-primary btn-sm btn-rounded" type="button">Restart</button>
                        </a>
                    </form>
                </div>
            </div>
        <?php endif;?>
        <?php
        //        var_dump($_SESSION['result']);
        ?>
        <?php if (isset($_SESSION['result'])):?>
            <?php if ($_SESSION['result'] != NULL):?>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead class="black white-text">
                            <tr>
                                <th scope="col">Average Waiting Time</th>
                                <th scope="col">Average Turn Around Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <?php
                                    $waitingTime = $_SESSION['result'][0];
                                    $averageWaitingTime = ($waitingTime*1.0)/$_SESSION['totalProcess'];
                                    echo '<h2>'.$averageWaitingTime.'</h2>';
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $turnAroundTime = $_SESSION['result'][1];
                                    $averageTurnAroundTime = ($turnAroundTime*1.0)/$_SESSION['totalProcess'];
                                    echo '<h2>'.$averageTurnAroundTime.'</h2>';
                                    ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <a href="reset.php">
                            <button class="btn btn-primary btn-sm btn-rounded" type="button">Reset</button>
                        </a>
                    </div>
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>

<?php include_once ('footer.php')?>