<?php
include_once ('Processes.php');
session_start();

$object = new Processes();
$totalProcess = $_SESSION['totalProcess'];
//var_dump($_POST);
if (!empty($_POST)){
    $object->set($_POST, $totalProcess);
}
if (isset($_POST['schedulingProcess'])){
    if ($_POST['schedulingProcess'] == 'rr'){
        $result = $object->rr();
//        var_dump($result);
        $_SESSION['result'] = $result;
        header('location: data.php');
//        var_dump($_SESSION['result']);
    }
    elseif ($_POST['schedulingProcess'] == 'sjf'){
        $result = $object->sjf();
        $_SESSION['result'] = $result;
        header('location: data.php');
    }
    elseif ($_POST['schedulingProcess'] == 'ps'){
        $result = $object->ps();
        $_SESSION['result'] = $result;
        header('location: data.php');
    }
}
//var_dump($result);
//var_dump($_POST, $_SESSION);