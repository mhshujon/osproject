<?php include_once ('header.php');
session_start();
?>

    <div class="container">
        <?php if (empty($_POST['totalProcess'])):?>
        <div class="row">
            <div class="col-md-12 offset-md-5">
                <form class="needs-validation container" action="data.php" method="POST">
                    <div class="form-row">
                        <div class="mb-3 md-form">
                            <select class="mdb-select" required name="totalProcess">
                                <option value="" disabled selected>Total Process</option>
                                <?php for ($i=1; $i<=10; $i++):?>
                                    <option value="<?php echo $i?>"><?php echo $i?></option>
                                <?php endfor;?>
                            </select>
                            <div class="invalid-feedback">Please select a country.</div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-sm btn-rounded" type="submit">Submit</button>
                </form>
            </div>
        </div>
        <?php endif;?>
    </div>

<?php include_once ('footer.php')?>