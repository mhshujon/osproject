<?php


class Processes
{
    private $arrivalTime = array();
    private $burstTime = array();
    private $remainingTime = array();
    private $remain;
    private $flag = 0;
    private $count;
    private $timeQuantam = 5;
    private $time;
    private $waitingTime=0;
    private $turnAroundTime=0;
    private $result = array();

    private $processNumber = array();
    private $tat = array();
    private $wt = array();
    private $pos;
    private $temp;

    private $pr = array();

    public function set($data = array(), $rt)
    {
        if ($data != NULL && $rt != NULL){
            $this->remain = $rt;
            $j = 1;
            for ($i=0; $i<$this->remain; $i++){
                $this->arrivalTime[$i] = $data['arrivalTimeP'.$j];
                $this->burstTime[$i] = $data['burstTimeP'.$j];
                $this->pr[$i] = $data['priority'.$j];
                $this->remainingTime[$i] = $this->burstTime[$i];
                $this->processNumber[$i] = $j;
                $j++;
            }
        }
//        var_dump($this->pr);
    }

    public function rr()
    {
        for ($this->time = 0, $this->count = 0; $this->remain!=0;){
            if ($this->remainingTime[$this->count]<=$this->timeQuantam && $this->remainingTime>0){
                $this->time+=$this->remainingTime[$this->count];
                $this->remainingTime[$this->count]=0;
                $this->flag=1;
            }
            elseif ($this->remainingTime[$this->count]>0){
                $this->remainingTime[$this->count]-=$this->timeQuantam;
                $this->time+=$this->timeQuantam;
            }
            if ($this->remainingTime[$this->count]==0 && $this->flag==1){
                $this->remain--;
                $this->waitingTime += ($this->time - $this->arrivalTime[$this->count]) - $this->burstTime[$this->count];
                $this->turnAroundTime += $this->time - $this->arrivalTime[$this->count];
                $this->flag=0;
            }
            if ($this->count==$this->remain-1){
                $this->count=0;
            }
            elseif ($this->arrivalTime[$this->count+1]<=$this->time){
                $this->count++;
            }
            else{
                $this->count=0;
            }
        }
        $this->result[0] = $this->waitingTime;
        $this->result[1] = $this->turnAroundTime;

        return $this->result;
    }

    public function sjf()
    {
        for ($i=0; $i<$this->remain;$i++){
            $this->pos = $i;
            for ($j=$i+1;$j<$this->remain;$j++){
                if ($this->burstTime[$j]<$this->burstTime[$this->pos])
                    $this->pos=$j;
            }
            $this->temp = $this->burstTime[$i];
            $this->burstTime[$i] = $this->burstTime[$this->pos];
            $this->burstTime[$this->pos] = $this->temp;

            $this->temp = $this->processNumber[$i];
            $this->processNumber[$i] = $this->processNumber[$this->pos];
            $this->processNumber[$this->pos] = $this->temp;
        }

        $this->wt[0] = 0;

        for ($i=1; $i<$this->remain;$i++){
            $this->wt[$i] = 0;
            for ($j=0;$j<$i;$j++){
                $this->wt[$i] += $this->burstTime[$j];
            }
            $this->waitingTime += $this->wt[$i];
        }

        for ($i=0; $i<$this->remain; $i++){
            $this->tat[$i] = $this->burstTime[$i] + $this->wt[$i];
            $this->turnAroundTime += $this->tat[$i];
        }
        $this->result[0] = $this->waitingTime;
        $this->result[1] = $this->turnAroundTime;

        return $this->result;

    }

    public function ps()
    {
        for ($i=0; $i<$this->remain; $i++){
            $this->pos = $i;
            for ($j=$i+1; $j<$this->remain; $j++){
                if ($this->pr[$j] < $this->pr[$this->pos]){
                    $this->pos = $j;
                }
            }

            $this->temp = $this->pr[$i];
            $this->pr[$i] = $this->pr[$this->pos];
            $this->pr[$this->pos] = $this->temp;

            $this->temp = $this->burstTime[$i];
            $this->burstTime[$i] = $this->burstTime[$this->pos];
            $this->burstTime[$this->pos] = $this->temp;

            $this->temp = $this->processNumber[$i];
            $this->processNumber[$i] = $this->processNumber[$this->pos];
            $this->processNumber[$this->pos] = $this->temp;
        }
        $this->wt[0] = 0;

        for ($i=1; $i<$this->remain; $i++){
            $this->wt[$i] = 0;
            for ($j=0; $j<$this->remain; $j++){
                $this->wt[$i] += $this->burstTime[$j];
            }
            $this->waitingTime += $this->wt[$i];
        }

        for ($i=0; $i<$this->remain; $i++){
            $this->tat[$i] = $this->burstTime[$i] + $this->wt[$i];
            $this->turnAroundTime += $this->tat[$i];
        }
        $this->result[0] = $this->waitingTime;
        $this->result[1] = $this->turnAroundTime;

        return $this->result;
    }
}